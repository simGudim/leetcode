use std::fmt::Debug;

#[derive(Debug)]
pub struct LinkedList<T>(Option<(T, Box<LinkedList<T>>)>);

impl <T> LinkedList<T> {
    pub fn new() -> Self {
        LinkedList(None)
    }

    pub fn push_front(&mut self, value: T) {
        let old_front = self.0.take();
        self.0 = Some((value, Box::new(LinkedList(old_front))));
    }

    pub fn push_back(&mut self, value: T) {
        match self.0 {
            Some((_, ref mut next)) => next.push_back(value),
            None => self.push_front(value)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut list = LinkedList::new();
        list.push_front(6);
        list.push_front(9);
        list.push_front(10);
        list.push_back(11);
        println!("{:?}", list);
        assert_eq!(3, 4);
    }
}
