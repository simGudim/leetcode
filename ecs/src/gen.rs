

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct GenData {
    pub pos: usize,
    pub gen: u64
}

pub struct EntityActive {
    active: bool,
    gen: u64
}

pub struct GenManager {
    items: Vec<EntityActive>,
    drops: Vec<usize>
}

impl GenManager {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
            drops: Vec::new()
        }
    }

    pub fn next(&mut self) -> GenData {
        if let Some(loc) = self.drops.pop() {
            let eu = &mut self.items[loc];
            eu.active = true;
            eu.gen += 1;
            return GenData {
                pos: loc,
                gen: eu.gen
            }
        }

        self.items.push(EntityActive{active: true, gen: 0});
        return GenData {
            pos: self.items.len() - 1,
            gen: 0
        }
    }

    pub fn drop(&mut self, g: GenData) {
        if let Some(eu) = self.items.get_mut(g.pos) {
            if eu.active && eu.gen == g.gen {
                eu.active = false;
                self.drops.push(g.pos);
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    fn test_items_drop() {
        let mut gm = GenManager::new();

        let g = gm.next();
        assert_eq!(g, GenData{pos: 0, gen: 0});
    }
}

