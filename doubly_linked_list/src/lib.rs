use std::{ cell::RefCell, rc::{Rc, Weak}};
use std::fmt::Debug;

#[derive(Debug)]
pub struct ListNode<T: Debug> {
    data: T,
    next: Option<Rc<RefCell<ListNode<T>>>>,
    prev: Option<Weak<RefCell<ListNode<T>>>>
}

#[derive(Debug)]
pub struct DoublyLinkedList<T: Debug> {
    front: Option<Rc<RefCell<ListNode<T>>>>,
    last: Option<Weak<RefCell<ListNode<T>>>>
}

impl <T: Debug> DoublyLinkedList<T> {
    pub fn new() -> Self {
        Self {
            front: None,
            last: None
        }
    }
    pub fn push_front(&mut self, data: T) {
        match self.front.take() {
            Some(front_node) => {
                let new_front = Rc::new(RefCell::new(ListNode{
                    data,
                    next: Some(front_node.clone()),
                    prev: None
                }));

                let mut old_front = front_node.borrow_mut();
                old_front.prev = Some(Rc::downgrade(&new_front));
                self.front = Some(new_front);


            }
            None => {
                let new_front = Rc::new(RefCell::new(ListNode{
                    data,
                    next: None,
                    prev: None
                }));
                self.front = Some(new_front.clone());
                self.last = Some(Rc::downgrade(&new_front));
            }
        }
    }

    pub fn push_back(&mut self, data: T) {
        match self.last.take() {
            Some(last_val) => {
                let new_last = Rc::new(RefCell::new(ListNode {
                    data,
                    next: None,
                    prev: Some(last_val.clone())
                }));   
                let last_val_strong = last_val.upgrade().unwrap();
                let mut last_val_borrow_mut = last_val_strong.borrow_mut();
                last_val_borrow_mut.next = Some(new_last.clone());
                self.last = Some(Rc::downgrade(&new_last));
            },
            None => {
                let new_last = Rc::new(RefCell::new(ListNode{
                    data,
                    next: None,
                    prev: None
                }));
                self.front = Some(new_last.clone());
                self.last = Some(Rc::downgrade(&new_last));
            }
        }
    }

    pub fn pop_front(&mut self) -> Option<T> {
        match self.front.take() {
            Some(front_val) => {
                match Rc::try_unwrap(front_val) {
                    Ok(refcell) => {
                        let deref_refcell = refcell.into_inner();
                        if let Some(ref next_node) = deref_refcell.next {
                            self.front = Some(next_node.clone());
                        } else {
                            self.front = None;
                        }
                        return Some(deref_refcell.data)
                    }
                    Err(_) => None
                }
            },
            None => {
                return None
            }
        }
    }

    pub fn pop_back(&mut self) -> Option<T> {
        match self.last.take() {
            Some(last_val) => {
                let last_val = last_val.upgrade().unwrap();
                let prev_val = Weak::upgrade(
                    last_val.borrow().prev.as_ref().unwrap()
                ).unwrap();
                prev_val.borrow_mut().next = None;
                self.last = Some(Rc::downgrade(&prev_val));
                match Rc::try_unwrap(last_val) {
                    Ok(val) => {
                        return Some(val.into_inner().data)
                    },
                    Err(_) => {
                        return None
                    
                    }
                }
            },
            None => return None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut list = DoublyLinkedList::new();
        list.push_front(11);
        list.push_front(12);
        list.push_back(19);
        list.push_back(100);
        list.push_front(101);
        list.pop_front();
        list.pop_front();
        let popped_val = list.pop_front();
        let back_popped = list.pop_back();
        println!("{:?}", back_popped);
        println!("{:?}", popped_val);
        println!("{:?}", list);
        assert_eq!(2, 4);
    }
}
