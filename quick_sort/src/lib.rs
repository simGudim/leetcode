use std::fmt::Debug;

pub fn pivot<T:PartialOrd + Debug>(a: &mut [T]) -> usize {
    let mut p = 0;
    for i in 1..a.len() {
        if a[i] < a[p] {
            a.swap(p + 1 , i);
            a.swap(p , p + 1);
            p += 1;
        }
    }
    p
}


pub fn quick_sort<T: PartialOrd + Debug>(v: &mut [T]) {
    if v.len() <= 1 {
        return
    }
    let p = pivot(v);
    let (a, b) = v.split_at_mut(p);
    quick_sort(a);
    quick_sort(&mut b[1..]);

}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut result = [3, 2, 1, 123, 0, 6436, 5, 22, 4, 7];
        quick_sort(&mut result);  
        assert_eq!(result, [0, 1, 2, 3, 4, 5, 7, 22, 123, 6436]);
    }
}
