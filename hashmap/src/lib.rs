mod hasher;
use std::{borrow::Borrow, hash::Hash};
pub use hasher::hash;

const BSIZE: usize = 8;
const BGROW: usize = 8;

#[derive(Debug)]
pub struct BucketList<K, V> {
    seed: u64,
    len: usize,
    buckets: Vec<Vec<(K, V)>>
}

impl <K: Hash + Eq, V> BucketList<K, V> {
    fn new() -> Self {
        Self {
            seed: rand::random(),
            len: 0,
            buckets: vec![Vec::new()]
        }
    }

    fn push(&mut self, k: K, v: V) -> usize {
        let h = (hash(self.seed, &k) as usize) % self.buckets.len();
        self.buckets[h].push((k, v));
        self.len += 1;
        return self.buckets[h].len()
    }

    pub fn get<KB>(&self, k: &KB) -> Option<&V> 
    where K: Borrow<KB>, 
        KB: Hash + Eq + ?Sized
    {
        let h = (hash(self.seed, &k) as usize) % self.buckets.len();
        for (key, value) in &self.buckets[h] {
            if key.borrow() == k {
                return Some(value)
            }
        }
        None
    }

    pub fn get_mut<KB>(&mut self, k: &KB) -> Option<&mut V> 
    where K: Borrow<KB>, 
        KB: Hash + Eq + ?Sized
    {
        let h = (hash(self.seed, &k) as usize) % self.buckets.len();
        for (key, value) in &mut self.buckets[h] {
            if (key as &K).borrow() == k {
                return Some(value)
            }
        }
        None
    }

    fn bucket(&mut self, n: usize) -> Option<Vec<(K, V)>> {
        if self.buckets.len() <= n {
            return None
        }

        let mut res = Vec::new();
        std::mem::swap(&mut res, &mut self.buckets[n]);
        self.len -= res.len();
        return Some(res)
    }

    fn set_bucket(&mut self, n: usize) {
        for _ in self.buckets.len()..n {
            self.buckets.push(Vec::new());
        }
    }
}

#[derive(Debug)]
pub struct HMmap<K, V>{
    n_moved: usize,
    main: BucketList<K, V>,
    grow: BucketList<K, V>
}

impl <K: Hash + Eq, V> HMmap<K, V> {
    pub fn new() -> Self {
        Self{
            n_moved: 0 ,
            main: BucketList::new(),
            grow: BucketList::new()
        }
    }

    pub fn insert(&mut self, k: K, v: V) {
        if let Some(iv) = self.main.get_mut(&k) {
            *iv = v;
            return
        }
        if let Some(iv) = self.grow.get_mut(&k) {
            *iv = v;
            return
        }

        if self.n_moved > 0 {
            self.grow.push(k, v);
            self.move_bucket();
            return
        }

        if self.main.push(k, v) > BSIZE / 2 {
            //grow buckets
            self.move_bucket()
        }
    }

    pub fn get_mut<KR>(&mut self, kr: &KR) -> Option<&mut V>
    where K: Borrow<KR>,
        KR: Hash + Eq + ?Sized
    {
        if let Some(b) = self.main.get_mut(kr) {
            return Some(b)
        }
        self.grow.get_mut(kr)
    }
    
    pub fn get<KR>(&mut self, kr: &KR) -> Option<&V>
    where K: Borrow<KR>,
        KR: Hash + Eq + ?Sized
    {
        self.main.get(kr).or_else(|| self.grow.get(kr))
    }

    pub fn len(&self) -> usize {
        self.main.len + self.grow.len
    }

    pub fn move_bucket(&mut self) {
        if self.n_moved == 0 {
            self.grow.set_bucket(self.main.len + BGROW);
        }
        if let Some(b) = self.main.bucket(self.n_moved) {
            for (k, v) in b {
                self.grow.push(k, v);
            }
            self.n_moved += 1;
            return
        }
        std::mem::swap(&mut self.main,&mut  self.grow);
        self.n_moved = 0;
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(3, 4);
    }
}
