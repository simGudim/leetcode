use std::fmt::Debug;

pub fn bubble_sort<T: PartialOrd + Debug>(array: &mut [T]) {
    for p in 0..array.len() {
        let mut sorted = true;
        for x in 0..(array.len() - 1) - p {
            println!("{:?}", array);
            if array[x] > array[x + 1] {
                array.swap(x, x + 1);
                sorted = false;
            }
        }
        if sorted {
            return
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bubble_sort_test() {
        let mut array = [1,4, 3, 7, 5, 10, 100, 50000, 6262, 123, 45734908, 22, 5];
        bubble_sort(&mut array);
        assert_eq!(array, [1,3, 4, 5, 5, 7, 10, 22, 100, 123, 6262, 50000, 45734908]);
    }
}
