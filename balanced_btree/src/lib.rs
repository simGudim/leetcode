use std::fmt::Debug;


#[derive(Debug)]
pub struct BTree<T>(Option<Box<BNode<T>>>);

#[derive(Debug)]
pub struct BNode<T> {
    data: T,
    height: i8,
    left: BTree<T>,
    right: BTree<T>
}


impl <T> BTree<T> {
    pub fn new() -> Self {
        BTree(None)
    }

    pub fn get_height(&self) -> i8 {
        match self.0 {
            Some(ref val) => val.height,
            None => 0
        }
    }

    pub fn set_height(&mut self) {
        if let Some(ref mut node) = self.0 {
            node.height = 1 + std::cmp::max(node.left.get_height(), node.right.get_height());
        }
    }

    pub fn rot_left(&mut self) {
        self.0 = self.0.take().map(|x| x.rotate_left());
    }

    pub fn rot_right(&mut self) {
        self.0 = self.0.take().map(|x| x.rotate_right());
    }
}

impl <T: PartialOrd> BTree<T> {
    pub fn add_sorted(&mut self, data: T) {
        let rot_dir = match self.0 {
            Some(ref mut bn) => {
                if data < bn.data {
                    bn.left.add_sorted(data);
                    if bn.left.get_height() < bn.right.get_height() - 1 {
                        1
                    } else {0}
                } else {
                    bn.right.add_sorted(data);
                    if bn.right.get_height() < bn.left.get_height() - 1 {
                        -1
                    } else {0}
                }
            },
            None => {
                self.0 = Some(Box::new(BNode{
                    data,
                    height: 0,
                    left: BTree::new(),
                    right: BTree::new()
                }));
                0
            }
        };
        match rot_dir {
            1 => self.rot_left(),
            -1 => self.rot_right(),
            0 => self.set_height(),
            _ => unreachable!()
        }
    }

}

impl <T: Debug> BTree<T> {
    pub fn print_lfirst(&self, dp: i32) {
        match self.0 {
            Some(ref bn) => {
                bn.left.print_lfirst(dp + 1);
                let mut spc = String::new();
                for _ in 0..dp {
                    spc.push_str(("."));
                }
                println!("{}:{}{:?}", bn.height, spc, bn.data);

                bn.right.print_lfirst(dp+1);
            },
            None => return
        }
    }
}

impl <T> BNode<T> {
    pub fn rotate_left(mut self) -> Box<Self> {
        if let Some(mut right_val) = self.right.0.take() {
            self.right = BTree(right_val.right.0.take());
            self.right.set_height();
            right_val.left = BTree(Some(Box::new(self)));
            right_val.left.set_height();
            right_val.height = 1 + std::cmp::max(right_val.left.get_height(), right_val.right.get_height());
            return right_val
        } else {
            return Box::new(self)
        }
    }

    pub fn rotate_right(mut self) -> Box<Self> {
        if let Some(mut left_val) = self.left.0.take() {
            self.left = BTree(left_val.left.0.take());
            self.left.set_height();
            left_val.right = BTree(Some(Box::new(self)));
            left_val.right.set_height();
            left_val.height = 1 + std::cmp::max(left_val.left.get_height(), left_val.right.get_height());
            return left_val
        } else {
            return Box::new(self)
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut btree = BTree::new();
        btree.add_sorted(70);
        btree.add_sorted(88);
        btree.add_sorted(99);
        btree.add_sorted(5);
        btree.add_sorted(6);
        btree.add_sorted(56);
        btree.add_sorted(8282);
        btree.add_sorted(9349349);
        btree.add_sorted(10101);
        btree.add_sorted(8);
        btree.add_sorted(11);
        btree.add_sorted(67);
        btree.add_sorted(50);
        btree.add_sorted(2);
        btree.print_lfirst(0);
        assert_eq!(2, 4);
    }
}
