use std::fmt::Debug;


#[derive(Debug)]
pub struct BTree<T>(Option<Box<BNode<T>>>);

#[derive(Debug)]
pub struct BNode<T> {
    data: T,
    left: BTree<T>,
    right: BTree<T>
}

impl <T> BTree<T> {
    pub fn new() -> Self {
        BTree(None)
    }
}

impl <T: PartialOrd> BTree<T> {
    pub fn add_sorted(&mut self, data: T) {
        match self.0 {
            Some(ref mut bn) => {
                if data < bn.data {
                    bn.left.add_sorted(data);
                } else {
                    bn.right.add_sorted(data);
                }
            },
            None => {
                self.0 = Some(Box::new(BNode{
                    data,
                    left: BTree::new(),
                    right: BTree::new()
                }));
            }
        }
    }
}

impl <T: Debug> BTree<T> {
    pub fn print_lfirst(&self, dp: i32) {
        match self.0 {
            Some(ref bn) => {
                if let Some(left_val) = &bn.left.0 {
                    left_val.left.print_lfirst(dp + 1);
                    let mut output = String::new();
                    for _  in 0..dp {
                        output.push('-');
                    }
                    println!("{}{:?}", output, left_val.data);
                }

                println!("{:?}", bn.data);

                if let Some(right_val) = &bn.right.0 {
                    right_val.right.print_lfirst(dp + 1);
                    let mut output = String::new();
                    for _  in 0..dp {
                        output.push('-');
                    }
                    println!("{}{:?}", output, right_val.data);
                }
            },
            None => return
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut btree = BTree::new();
        btree.add_sorted(10);
        btree.add_sorted(88);
        btree.add_sorted(99);
        btree.add_sorted(5);
        btree.print_lfirst(0);
        assert_eq!(2, 4);
    }
}
