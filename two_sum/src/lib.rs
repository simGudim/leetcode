pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32>  {
    for (v, i) in (0i32..).zip(&nums) {
        for (v2, i2) in (0i32..).zip(&nums)  {
            let sum = i+i2;
            println!("{}+{}={}", v, v2, sum);
            if sum == target  && v != v2{
                return vec![v, v2]
            }
        }
    }
    vec![]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let nums = vec![2,7,11,15];
        let result = two_sum(nums, 9);
        assert_eq!(result, vec![0,1]);
    }
}
