use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::rc::Rc;
use std::fmt::{Debug, Display};
use rand::prelude::*;

pub trait Weighted {
    fn weight(&self) -> i32;
}

impl Weighted for i32 {
    fn weight(&self) -> i32 {
        *self
    }
}

#[derive(Debug)]
pub struct Route<ID: Eq + Debug> {
    pos: ID,
    path: Option<Rc<Route<ID>>>,
    len: i32
}

impl <ID: Eq + Debug>Route <ID> {
    pub fn start_rc(pos: ID) -> Rc<Self> {
        Rc::new(Self {
            pos,
            path: None,
            len: 0
        })
    }

    pub fn contains(&self, id: ID) -> bool {
        if self.pos == id {
            return true
        }

        match &self.path {
            Some(p) => p.contains(id),
            None => return false
        }
    }
} 

impl <ID: Debug + Eq> Display for Route<ID> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(ref p) = self.path {
            write!(f, "{}-{}", p, self.len)?;
        } 
        write!(f, "{:?}", self.pos)
    }
}

#[derive(Debug)]
pub struct GraphErr {
    mess: String
}

impl GraphErr {
    pub fn new(s: &str) -> Self {
        Self {
            mess: s.to_string()
        }
    }
}

#[derive(Debug)]
pub struct Graph<T, E, ID: Hash + Eq> {
    data: HashMap<ID, (T, Vec<ID>)>,
    edges: HashMap<ID, (E, ID, ID)>
}

impl <T, E, ID: Clone + Hash+ Eq> Graph <T, E, ID>{
    pub fn new() -> Self {
        Self {
            data : HashMap::new(),
            edges: HashMap::new()
        }
    }

    pub fn add_node(&mut self, id: ID, dt: T) {
        self.data.insert(id, (dt, Vec::new()));
    }

    pub fn add_edge(&mut self, edge_id: ID, from: ID, to: ID, edge_cost: E) -> Result<(), GraphErr> {
        if ! self.data.contains_key(&from) {
            return Err(GraphErr::new("from id is not in the data nodes"))
        }

        if let Some(ref mut to_node) = self.data.get_mut(&to) {
            self.edges.insert(edge_id.clone(), (edge_cost, from.clone(), to));
            to_node.1.push(edge_id.clone());
        }

        self.data.get_mut(&from).unwrap().1.push(edge_id);
        Ok(())
    }
}

impl <T, E: Weighted, ID: Clone + Hash + Eq + Debug> Graph <T, E, ID> {
    pub fn shortest_path(&self, from: ID, to: ID) -> Option<Rc<Route<ID>>> { 
        self.shortest_path_r(Route::start_rc(from), to)
    }

    pub fn shortest_path_r(&self, from: Rc<Route<ID>>, to: ID) -> Option<Rc<Route<ID>>> { 
        let mut toset = HashSet::new();
        toset.insert(to);
        self.closest(from, &toset)
    }

    pub fn closest(&self, from: Rc<Route<ID>>, to: &HashSet<ID>) -> Option<Rc<Route<ID>>> {
        let mut visited = HashSet::new();
        let mut routes = Vec::new();
        
        routes.push(from);

        loop {
            let c_route = routes.pop()?;
            if to.contains(&c_route.pos) {
                return Some(c_route);
            }

            if visited.contains(&c_route.pos) {
                continue;
            }
            visited.insert(c_route.pos.clone());
            let exists = self.data.get(&c_route.pos)?;
            for edge_id in &exists.1 {
                let edge = self.edges.get(edge_id)?;
                let opos = if edge.1 == c_route.pos {
                    edge.2.clone()
                } else {
                    edge.1.clone()
                };
                let opos_len = c_route.len + edge.0.weight();
                let oroute = Rc::new(Route{
                    pos: opos,
                    len: opos_len,
                    path: Some(c_route.clone())
                });
                
                if routes.len() == 0 {
                    routes.push(oroute);
                    continue;
                }

                let mut iafter = routes.len() -1;
                loop {
                    if routes[iafter].len > opos_len {
                        routes.insert(iafter + 1,oroute);
                        break;
                    } 

                    if iafter == 0 {
                        routes.insert(0, oroute);
                        break;
                    }
                    iafter -= 1;
                }
            }

        }
    }

    pub fn greedy_salesman(&self, start: ID) -> Option<Rc<Route<ID>>> {
        let mut to_visit_nodes: HashSet<ID> = self.data.keys()
            .filter(|x| *x != &start)
            .map(|x| x.clone())
            .collect();
        let mut route = Route::start_rc(start.clone());
        while to_visit_nodes.len() > 0 {
            route = self.closest(route, &to_visit_nodes)?;
            to_visit_nodes.remove(&route.pos);
        }
        self.shortest_path_r(route, start)
    }

    pub fn complete_path(&self, path: &[ID]) -> Option<Rc<Route<ID>>> {
        if path.len() < 2 {
            return None
        }

        let mut route = Route::start_rc(path[0].clone());
        for pos in &path[1..path.len() - 1] {
            if !route.contains(pos.clone()) {
                route = self.shortest_path_r(route, pos.clone())?;
            }
        } 

        self.shortest_path_r(route, path[path.len() -1].clone())
    }

    pub fn iter_salesman(&self, start: ID) -> Option<Rc<Route<ID>>> {
        let mut bpath: Vec<ID> = self.data.keys().map(|x| x.clone()).collect();
        bpath.shuffle(&mut rand::thread_rng());

        for i in 0..bpath.len(){
            if bpath[i] == start {
                bpath.swap(0, i);
                break;
            }
        }
        bpath.push(start);
        let mut broute = self.complete_path(&bpath)?;
        let mut no_imp = 0;

        loop {
            let mut p2 = bpath.clone();
            let sa = (rand::random::<usize>() % (p2.len() - 2)) + 1;
            let sb = (rand::random::<usize>() % (p2.len() - 2)) + 1;
            p2.swap(sa, sb);

            let r2 = self.complete_path(&p2)?;
            if r2.len < broute.len {
                bpath = p2;
                broute = r2;
                no_imp = 0;
            }
            no_imp += 1;
            if no_imp >= 50 {
                return Some(broute)
            }
        }

    }

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_graph() {
        let mut graph = Graph::new();

        for (i, v) in vec!['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'].into_iter().enumerate() {
            graph.add_node(v, i);
        }

        graph.add_edge('a', 'H', 'D', 6).unwrap();
        graph.add_edge('b', 'D', 'C', 18).unwrap();
        graph.add_edge('c', 'C', 'B', 10).unwrap();
        graph.add_edge('d', 'H', 'A', 7).unwrap();
        graph.add_edge('e', 'A', 'C', 4).unwrap();
        graph.add_edge('f', 'H', 'G', 5).unwrap();
        graph.add_edge('g', 'G', 'A', 8).unwrap();
        graph.add_edge('h', 'A', 'F', 3).unwrap();
        graph.add_edge('i', 'F', 'E', 15).unwrap();
        graph.add_edge('j', 'C', 'E', 12).unwrap();

        println!("Hello graph {:?}", graph);
        println!("shortest path A-D {:?}", graph.shortest_path('A', 'D').unwrap());
        println!("shortest path C-F {:?}", graph.shortest_path('C', 'F').unwrap());
        assert_eq!(1, 4);
    }
}
