use core::fmt;
use std::{cell::RefCell, rc::Rc};
use std::fmt::{Debug, Display, Write};


type Rcc<T> = Rc<RefCell<T>>;

#[derive(Debug)]
pub struct SkipNode<T: PartialOrd> {
    right: Option<Rcc<SkipNode<T>>>,
    down: Option<Rcc<SkipNode<T>>>,
    data: Rcc<T>
}

pub struct SkipList<T: PartialOrd>(Vec<SkipNode<T>>);

impl <T:PartialOrd> SkipList<T> {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn insert(&mut self, data: T) {
        if self.0.len() == 0 {
            self.0.push(SkipNode::new(data));
            return
        }

        for i in (0..self.0.len()).rev() {
            if data > *self.0[i].data.borrow() {
                if let Some(child) = self.0[i].insert(data) {
                    self.loop_up(child, i + 1);
                };
                return
            }
        }

        let mut nn = SkipNode::new(data);
        std::mem::swap( &mut nn, &mut self.0[0]);
        let res = rcc(nn);
        self.0[0].right = Some(res.clone());
        self.loop_up(res, 1);
    }

    pub fn loop_up(&mut self, child: Rcc<SkipNode<T>>, n: usize) {
        if rand::random::<bool>() == true {
            return;
        }

        let dt = child.borrow().data.clone();
        let mut nn = SkipNode {
            data: dt,
            right: None,
            down: Some(child)
        };

        if n >= self.0.len() {
            self.0.push(nn);
            return;
        }

        std::mem::swap(&mut nn, &mut self.0[n]);
        let res = rcc(nn);
        self.0[n].right = Some(res.clone());
        self.loop_up(res, n + 1);

    }
}

impl <T: Debug + PartialOrd> Display for SkipList<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0.len() == 0 {
            return write!(f, "SkipList<Empty");
        }

        for i in &self.0 {
            write!(f, "\n");
            i.print_row(f)?;
        }

        Ok(())
    }
}

pub fn rcc<T>(t: T) -> Rcc<T> {
    return Rc::new(RefCell::new(t))
}


impl <T: PartialOrd>SkipNode<T> {
    pub fn new(t: T) -> Self {
        Self {
            right: None,
            down: None,
            data: rcc(t)
        }
    }

    pub fn insert(&mut self, dt: T) -> Option<Rcc<SkipNode<T>>>{
        if let Some(ref mut right_val) = self.right {
            if dt > *right_val.borrow().data.borrow() {
                return right_val.borrow_mut().insert(dt)
            }
        }

        if let Some(ref down_val) = self.down {
            return match down_val.borrow_mut().insert(dt) {
                Some(child) => match rand::random::<bool>() {
                    true =>{
                        let dt = child.borrow().data.clone();
                        let nn = SkipNode {
                            right: self.right.take(),
                            down: Some(child),
                            data: dt
                        };
                        let res = rcc(nn);
                        self.right = Some(res.clone());
                        return Some(res)
                    },
                    false => None
                },
                None => return None
            }
        }

        let old_right = self.right.take();
        let new_right = SkipNode {
            right: old_right,
            down: None,
            data: rcc(dt)
        };
        let res = rcc(new_right);
        self.right = Some(res.clone());
        Some(res)
    }
}

impl <T: Debug + PartialOrd> SkipNode<T> {
    pub fn print_row<W: Write>(&self, w: &mut W) -> std::fmt::Result {
        write!(w, "{:?}", self.data.borrow())?;
        if let Some(ref right_val) = self.right {
            write!(w, ",");
            right_val.borrow().print_row(w)?;
        }
        Ok(())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        // let result = add(2, 2);
        let mut snode = SkipList::new();
        snode.insert(10);
        snode.insert(6);
        snode.insert(100);

        println!("s = {}", snode);
        assert_eq!(3, 4);
    }
}
