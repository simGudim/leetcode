use bincode::config::BigEndian;
use failure_derive::*;

#[derive(Fail, Debug)]
pub enum BlolbError {
    #[fail(display = "No Room")]
    NoRoom,
    #[fail(display = "Too Big {}", 0)]
    TooBig(u64),
    #[fail(display = "Not Found")]
    NotFound,
    #[fail(display = "BinCode {}", 0)]
    BinCode(bincode::Error),
    #[fail(display = "IO {}", 0)]
    IO(std::io::Error)
}

impl From<bincode::Error> for BlolbError {
    fn from(e: bincode::Error) -> Self {
        BlolbError::BinCode(e)
    }
}

impl From<std::io::Error> for BlolbError {
    fn from(e: std::io::Error) -> Self {
        BlolbError::IO(e)
    }
}