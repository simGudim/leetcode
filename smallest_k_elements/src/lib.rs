// You are working as a spy in an airbase during a war. On your computer, there is a list of targets. You are aware that the general will order strikes on the first few targets given to him.
// However, you need to act quickly before one of his advisors provides targets that would actually harm your forces.
// You have the ability to compare the targets and determine which ones would have a greater or lesser impact on the war effort.
// Your mission, if you choose to accept it, is to create a list that contains the K targets which would cause the least harm to your forces, based on the information available on your computer.

// Task
// Your task is to write a function that takes an unsorted list of comparable values and a number K, representing the number of targets (ammo) to return. The function should return a list containing the K smallest values from the input list.

// For example, given the list [1, 5, -3, 2] and K = 2, the function should return one of these lists: [-3, 1], [1, -3]. for k = 0 return an empty list

// Performance specifications:
// 0 <= k <= length
// sorting the input will be too slow, this is a preformance kata

// there will be 3 tests with lists sized: 1,000,000  
// there will be 3 tests with lists sized: 5,000,000  
// there will be 3 tests with lists sized: 20,000,000  
// there will also be more tests with smaller lists

// small note: there is a function in the standard library that solves this, so I check you don't use it in the main test :).

pub fn pivot<T: Copy + Ord + PartialOrd>(arr: &mut [T]) -> usize {
    let mut p = 0;
    for i in 1..arr.len() {
        if arr[p] > arr[i] {
            arr.swap(p+1, i);
            arr.swap(p,  p + 1);
        }
    }
    p
}

fn get_k_smallest<T: Copy + Ord + PartialOrd>(arr: &mut [T], k: usize) -> Vec<T> {
    let pivot_index = pivot(arr);
    let (arr_a, arr_b) = arr.split_at_mut(pivot_index);

    if arr_a.len() == k {
        return arr_a.to_vec()
    }
    get_k_smallest(arr_a, k);
    get_k_smallest(arr_b, k);
    return arr[0..3].to_vec()
}

#[cfg(test)]
mod tests {
    use super::get_k_smallest;

    #[test]
    fn sample_tests() {
        test_correctness_helper(&mut [1, 2, 3], 0); // []
        test_correctness_helper(&mut [1, 2, 3], 1); // [1]
        test_correctness_helper(&mut [1, 2, 3], 2); // [1,2]
        test_correctness_helper(&mut [1, 2, 3], 3); // [1,2,3]

        test_correctness_helper(&mut [3, 2, 1], 1); // [1]
        test_correctness_helper(&mut [3, 2, 1], 2); // [1,2]
        test_correctness_helper(&mut [3, 2, 1], 3); // [1,2,3]

        test_correctness_helper(&mut [1, 1, 1], 3); // [1,1,1]
        test_correctness_helper(&mut [1, 1, 1], 2); // [1,1]

        test_correctness_helper(&mut [1, -5, 1], 2); //[-5,1]
        test_correctness_helper(&mut [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 2); // [1,2]
        test_correctness_helper(&mut [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 9); // [1,2,3,4,5,6,7,8,9]
    }

    fn test_correctness_helper(arr: &mut [i64], k: usize) {
        let mut arr_sorted: Vec<i64> = arr.to_vec();
        arr_sorted.sort_unstable();
        // this is the sorted arr, so the first K elements here are also the smallest
        let expected: Vec<i64> = arr_sorted.iter().take(k).copied().collect();
        let ans = get_k_smallest(arr, k);
        let mut ans_sorted = ans.clone();
        ans_sorted.sort();
        // I also sort your answer, because you can give the answers in whatever order you want
        println!("sorted arr: {arr_sorted:?}, your answer: {ans:?}\nyour answer after sorting: {ans_sorted:?}, expected answer: {expected:?}\n");
        assert_eq!(ans_sorted, expected);
    }
}
