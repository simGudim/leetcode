use std::collections::BTreeMap;
use std::fmt::Debug;

#[derive(Debug)]
pub enum HuffNode {
    Tree(Box<HuffNode>, Box<HuffNode>),
    Leaf(char)
}

pub struct HScore {
    h: HuffNode,
    s: i32
}

pub fn build_tree(s: &str) -> HuffNode {
    let mut map = BTreeMap::new();
    for c in s.chars() {
        let n = *map.get(&c).unwrap_or(&0);
        map.insert(c, n + 1);
    }

    let mut tlist: Vec<HScore> = map
        .into_iter()
        .map(|(k, s)| HScore {
            h: HuffNode::Leaf(k),
            s
        }).collect();

    while tlist.len() > 1 {
        let last = tlist.len() - 1;
        for i in 0..tlist.len() - 1 {
            if tlist[i].s < tlist[last - 1].s {
                tlist.swap(i, last - 1);
            }

            if tlist[last - 1].s < tlist[last].s {
                tlist.swap(last - 1, last);
            }
        }

        let a_node = tlist.pop().unwrap();
        let b_node = tlist.pop().unwrap();

        let nnode = HuffNode::Tree(Box::new(a_node.h), Box::new(b_node.h));
        tlist.push(HScore{
            h: nnode,
            s: a_node.s + b_node.s
        });
    }
    tlist.pop().unwrap().h
}

impl HuffNode {
    pub fn print_lfirst(&self, depth: i32, dir: char) {
        match self {
            HuffNode::Tree(l, r) => {
                l.print_lfirst(depth + 1, '/');
                let mut spc = String::new();
                for _ in 0..depth {
                    spc.push('.');
                }
                println!("{}{}*", spc, dir);
                r.print_lfirst(depth + 1, '\\');
            },
            HuffNode::Leaf(c) => {
                let mut spc = String::new();
                for _ in 0..depth {
                    spc.push('.');
                }
                println!("{}{}{}", spc, dir, c);
            }
        }
    }

    pub fn encode_char(&self, c: char) -> Option<Vec<char>> {
        match self {
            HuffNode::Tree(l,r) => {
                if let Some(mut left) = l.encode_char(c) {
                    left.insert(0, '0');
                    return Some(left)
                } if let Some(mut right) = r.encode_char(c) {
                    right.insert(0, '1');
                    return Some(right)
                }
                None
            },
            HuffNode::Leaf(nc) => {
                if c == *nc {
                    return Some(Vec::new())
                }
                None
            }
        }
    }

    pub fn encode_str(&self, s: &str) -> Option<Vec<Vec<char>>> {
        let mut res = Vec::new();
        for i in s.chars() {
            let encoded_char = self.encode_char(i)?;
            res.push(encoded_char);
        }
        Some(res)
    }

    pub fn decode_char(&self, ec: Vec<char>, dir_index: usize) -> HuffNode {
        match self {
            HuffNode::Tree(l, r) => {
                let dir = ec[dir_index];
                if dir == '0' {
                    return  l.decode_char(ec,  dir_index + 1)
                } else {
                    return r.decode_char(ec,  dir_index + 1)
                } 
            }
            HuffNode::Leaf(c) => {
                return HuffNode::Leaf(*c)
            }
        }
    }

    pub fn decode_str(&self, s: Vec<Vec<char>>) -> Option<String> {
        let mut res = String::new();
        for encoded_char in s {
            let huff_leaf = self.decode_char(encoded_char, 0);
            match huff_leaf {
                HuffNode::Leaf(c) => res.push(c),
                HuffNode::Tree(_, _) => unreachable!()
            }
        }
        Some(res)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let s = "at an apple app, I am Phi";
        let huff_tree = build_tree(s);
        println!("{}", &s);
        huff_tree.print_lfirst(0, '<');

        println!("{:?}", huff_tree);

        let encodede_str = huff_tree.encode_str(s).unwrap();
        println!("{:?}", encodede_str);

        let decoded_string = huff_tree.decode_str(encodede_str).unwrap();
        println!("{:?}", decoded_string);
        assert_eq!(3, 4);
    }
}
