use std::fmt::Debug;


pub fn merge_sort<T: PartialOrd + Debug + Clone>(mut v: Vec<T>) -> Vec<T> {
    println!("MS: {:?}", v);
    if v.len() < 2 {
        return v
    }
    
    let mut result = Vec::with_capacity(v.len());
    let b = v.split_off(v.len() / 2);
    let a = merge_sort(v);
    let b = merge_sort(b);

    let mut a_it = a.into_iter();
    let mut b_it = b.into_iter();
    let mut a_peak = a_it.next();
    let mut b_peak = b_it.next();

    loop {
        println!("result: {:?}", &result);
        match a_peak {
            Some(ref a_val) => {
                match b_peak {
                    Some(ref b_val) => {
                        if a_val < b_val {
                            result.push(a_peak.take().unwrap());
                            a_peak = a_it.next();
                        } else {
                            result.push(b_peak.take().unwrap());
                            b_peak = b_it.next();
                        }
                    },
                    None => {
                        result.push(a_peak.take().unwrap());
                        result.extend(a_it);
                        return result
                    }
                }
            },
            None => {
                if let Some(b_val) = b_peak {
                    result.push(b_val);
                }
                result.extend(b_it);
                return result
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn merge_sort_test() {
        let array = vec![1,4, 3, 7, 5, 10, 100, 50000, 6262, 123, 45734908, 22, 5];
        let array = merge_sort(array);
        assert_eq!(array, vec![1,3, 4, 5, 5, 7, 10, 22, 100, 123, 6262, 50000, 45734908]);
    }
}
