// Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

// The overall run time complexity should be O(log (m+n)).

 

// Example 1:

// Input: nums1 = [1,3], nums2 = [2]
// Output: 2.00000
// Explanation: merged array = [1,2,3] and median is 2.
// Example 2:

// Input: nums1 = [1,2], nums2 = [3,4]
// Output: 2.50000
// Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
 

// Constraints:

// nums1.length == m
// nums2.length == n
// 0 <= m <= 1000
// 0 <= n <= 1000
// 1 <= m + n <= 2000
// -106 <= nums1[i], nums2[i] <= 106



struct Solution;

impl Solution {
    pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
        let num1_len = nums1.len();
        let num2_len = nums2.len();

        if num1_len == 0 && num2_len == 0 {
            return 0.0
        } else if num1_len > 0 && num2_len == 0 {
            let median = num1_len / 2;
            if num1_len % 2 != 0 {
                return nums1[median].into()
            } else {
                let upper = nums1[median] as f64;
                println!("upper: {}", upper);
                let lower = nums1[median-1] as f64;
                println!("lower: {}", lower);
                return (lower + upper) / 2.0
            }
        } else if num1_len == 0 && num2_len > 0 {
            let median = num2_len / 2;
            if num2_len % 2 != 0 {
                return nums2[median].into()
            } else {
                let upper = nums2[median] as f64;
                println!("upper: {}", upper);
                let lower = nums2[median-1] as f64;
                println!("lower: {}", lower);
                return (lower + upper) / 2.0
            }
        }
        2.000
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = Solution::find_median_sorted_arrays(vec![1,2,3], vec![]);
        assert_eq!(2.000, result);
    }


    #[test]
    fn it_works_odd() {
        let result = Solution::find_median_sorted_arrays(vec![1,2,3,4], vec![]);
        assert_eq!(2.5, result);
    }

    #[test]
    fn it_works_odd2() {
        let result = Solution::find_median_sorted_arrays(vec![], vec![1,2,3,4]);
        assert_eq!(2.5, result);
    }
}
